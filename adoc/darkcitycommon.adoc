ifdef::backend-html5[]
:full-width: width='100%'
:half-width: width='50%',align="center"
:thumbnail: width='60'
endif::[]
ifdef::backend-pdf[]
:full-width: pdfwidth='100%'
:half-width: pdfwidth='50%',align="center"
:thumbnail: pdfwidth='20mm'
endif::[]