﻿![](img/mrbook_grey.jpeg)

# Dark City Role Playing Game

A Role Playing Setting based on the Movie Dark City, build with the Cortex Prime System. Fan Content by John Karr 2021. Humans and Strangers strive to set a new course for the City after Murdoch seized control of the Machine.

The following Cortex Prime features are used: Roles, Distinctions, Attributes, Callbacks, Complications, Relations (called Connections), Powers, and Assets. In addition there is a special Tuning Ability which is scaled from 0 to infinity instead of the standard Cortex D4 to D12 scale. 

![](img/sleep_now_grey.png)

\pagebreak

# Attributes:

### CHARM

* persuading and making impressions

### INTELLECT

* learning and reasoning

### VITALITY

* strength and endurance

### WILL

* determination. resisting and using mental control powers.

### WITS

* quick thinking and perception

# Assets and Props

Assets and props can be obtained on the fly by expending a plot point or as a consequence of a scene’s resolution. Signature Assets may be obtained by expenditure of a experience whether earned or available at character creation. When lost a Signature Asset will be replaced with a similar item at the beginning of the next story arc, Signature Asset can be things which would otherwise be harder to get. 

At creation characters may take a reasonable selection for their role of assets and props such as vehicles and weapons. For example a character with the Role of Underworld Enforcer would always have access to small arms without needing a signature asset, if a Bookseller always wants access to better weapons than are found in a typical kitchen they need the signature asset.

# Complications

While most complications can be bought off with Plot Points over time, some cannot. If a complication is permanent it can only be removed with experience, if it is resolved in the Story a new complication must be taken unless experience is spent.

\begin{wrapfigure}[15]{L}{0.3\textwidth}
\includegraphics[width=0.25\textwidth]{img/bumstead_accordian_bw.png}

\end{wrapfigure}
# Callbacks

Dark City uses Callbacks. Once a session is used for experience it is no longer available for Callback.

## Moment

Moment is a special Callback that each Human has. A cherished (false) memory from their imprinting. For Detective Bumstead it was his accordion, for the clerk at the desk when Murdoch woke up it was ‘No time off for good behavior’, for Murdoch, Shell Beach. Once per session the player may invoke their Moment as a Callback to spend as a plot point.

# Awake and Subject Characters

A character who is still a subject of their imprinting may be referred to as a Subject, as in Subject of the Experiment. A character who is aware of the Experiment may be referred to as Awake or Wakeful. 

# Imprinting

The library from which the imprints were drawn was destroyed. Schreber is working on repairing the lab in the hopes that humans could use the process to their advantage.

Cross Imprinting between Strangers and Humans has never succeeded. The subject has always gone completely mad within a few days.

\clearpage

\begin{wrapfigure}{L}{0.3\textwidth}
\includegraphics[width=0.25\textwidth]{img/bumstead_grey.png}

\end{wrapfigure}

# Roles

Each human was imprinted with a Role: Policeman, School Teacher, Killer and so on. They are assumed to have appropriate skills for the Role. Roles are always at D8. In addition the Role gives the character either 2 D6 connections or 1 D8 connection. These must fit the primary role.

Characters may have been imprinted with multiple plotlines, but they gain only one Role and must use other sources to develop any secondary Roles or plotlines.

# Distinctions

Characters have 3 distinctions.

## Distinction: Soul

For lack of a better term this is the original essence of the person. This must reflect the person sans imprint. If a character is re-imprinted they will keep their Soul distinction. 

## Distinction: Role

The role distinction must reflect the Role that was selected for the person on their last imprint.

## Distinction: Open

The source of this distinction may be the character’s role, their soul, or their experience after awakening.

\clearpage

# Connections (Relations)

In additional to connections from their Role, characters start with an additional single connection at D6, there must be a reason for this connection to exist, but it does not have to come from their primary role. If the player does not know the connection they want at character creation they may defer and spend a plot point to create it during play.  

A connection above D8 is rare.

Remember Connections are a two way street, they may ask things of you too. 

## Contacts

Contacts are a limited form of connection. They apply to a specific group of people instead of 1 person. Contacts may be called on for information that might be common within the group and for introductions to other members of the group. For more substantial favors they need to be broken out into individual contacts.

## Connections on the Fly

A player can expend a plot point to initiate a D4 connection with another character. This relationship is limited by imprinted roles and recent activity, it may be temporarily stepped up further with a plot point, or permanently stepped up by spending experience.

## Prior Life Connections

A character may also offer a plot point to the GM to establish a Prior Life Connection to another character once per session, this is not limited by roles or recent activity. If the character is controlled by the GM, they may accept or decline the plot point, if they are a player character they may accept and pay the GM a plot point as well. GM characters may also offer player characters a plot point to establish Prior Life Connection.

When a prior life relationship is established it begins at D6, if experience is spent in place of plot point the relationship begins at D8. 

If a character refuses an offer for the prior life relationship, this an opportunity for the GM to create a D4 complication that the other character dislikes them. For each additional plot point the GM gives the player, the GM may step up the complication that the other hates the player or that the player likes the other. If a player initiates prior life connection with a Stranger the GM may assign a complication that the body belonged to a Prior Life Connection, which may either be that the character likes the Stranger anyway or that the character is upset by the Stranger in that body.

If a wakeful character has a connection to someone who is re-imprinted, the connection is complicated by the fact that the other no longer remembers them until they can re-establish the connection.

> John Murdoch had the Role: Killer. He had Emma his wife as a connection from his Role at D8, and Uncle Karl as his other connection at D6. When Emma was re-imprinted as Anna, John initiated a Prior Life Connection, spending a plot point to which she responded with an experience point to restore the D8 connection the two had. If she had refused John would have taken a complication for the incomplete connection.

\pagebreak

![](img/strangers_clock_bw.png)

> __The massive metal face loomed in the darkness. Its sculpted iron features gazed impassively at existence, like some ancient idol.__

> __Slowly, the brooding eyes parted and the cold metal features seemed to peel away, revealing a giant clock. Its deep, ominous ticking echoed across the gloom.__

> __A gloved hand reached out of the blackness, wrapped around a steel scissor switch, and yanked.__

> __At once the clock's gleaming second hand stopped dead--and the world fell silent...__

# Tuning

The roll to resist Tuning (either the City shutting down or a Stranger power) is WILL + TUNING, additional dice may not be added if the TUNING rating is 0, with a rating of D2 it becomes possible to bring other Dice into a resistance roll. For ratings above D12 the pluses are added to the TUNING die (which can effectively no longer roll 1). To notice changes that occurred from a TUNING or the inconsistencies of the world is the greater of WITS or INTELLECT + TUNING vs 2D6, with an effect die of D6 required; only with the help of aware characters working hard to help them see can a character with a TUNING of 0 have even a hope of understanding.

The rating scale for TUNING is: 

|  |  |    |    |    |    |    |    |    |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| 0 | D2 | D3 | D4 | D6 | D8 | D10 | D12 | +1 per additional rating. |



