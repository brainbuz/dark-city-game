﻿
# The City

The City is laid out in Concentric Circles, it is 3 miles across. Murdoch has attached a sea and a small spit of land on which to place Shell Beach. Much of the City is empty. There are several canals crossing the City as well, dividing sections of the city, several of which are completely empty.

## Transit and Roads

The map in the movie does not reflect system layout. There is a ring line which runs around the City, the ring line has double track and the express circles the city without stopping. There are several lines that cut in curvy yin-yang shapes through the center. 

Main street spirals through the City, to drive through it mostly one has to follow main, except for the crosstown which cuts across the city, but avoids the innermost rings.  

## Arts and Entertainment

Two movie theaters were shown in the movie. The Fremont is a large grand movie house, where Anna was working at the end of Dark City. There was also a shot of a different smaller un-named movie house elsewhere in the City. There is no TV or radio. The movies are on film, and originated from our world. Books, movies, records cut off around 1970. 

The Subjects do not have a clear concept of earth or historical timelines. Researching the media is likely to be a project for the increasing numbers of awakened.

## Commerce and Industry

Most real production is performed by the machine. Stores and Service business operate. Most businesses are a show put on to persuade the employees that they have job. Trucks drive up to loading docks and pickup food and goods for distribution, leaving manufactured output of the humans to be recycled by the machine. 

![](img/metrailmap.png)

