﻿
# Character Creation

## Attributes

Humans start with D4 in all attributes.

Strangers start with D4 in their attributes, except CHARM which starts at D3. 

Characters gain 8 steps up but may not raise an attribute above D10. Strangers may not raise CHARM above D8. Strangers may step VITALITY to D3 to step up another attribute or Tuning.

## Distinctions

A Character’s first Distinction is their Soul Distinction, which would survive imprinting. Strangers share a single Soul Distinction.

The second must relate to their Role. 

The Third is less constrained.

## Moment

Human Characters have a moment. Strangers do not.

## Connection

Human Characters have 1 Connection at D6. Human Characters have an additional Role based Connection at D8, or two at D6. For up to 3 starting Connection.

Strangers do not get connections. Strangers with the Agent Role, get 2 assets of D6 for humans who’ve been imprinted with them as a Connection. So long as the Human remains a Subject and not Awakened, complications that arise from rejection are always temporary.

Characters have 1 connection at D6.

Characters start with a D3 TUNING, they may exchange the steps to step up other stats.

Characters may be given an initial allotment of experience that they can hold or expend immediately.

\pagebreak

# Additional Systems

## Taking Half Instead of Rolling Dice

Instead of rolling you may take half value for all dice greater than D6 and count D6 as 2. Plus values are added directly to the associated 6 (D12) when doing this. At least 3 dice must be used, none of them may be less than D6. It is not permitted to roll some and halve others. 

## Plus Dice

Tuning potentially goes above D12, the extra advances are represented as plus values (+1 per advance). When taking half, the full plus is added to the die. The plus is added to the Tuning die, which means that hitches are no longer possible on that die (but may still come from other dice). 

## Stress

In adverse situations such as combat Complications are awarded as stress to an attribute, VITALITY in physical conflict and WILL in tuning conflicts. When the stress on an attribute reaches the attribute the character is taken out of the scene. 

# Experience

Experience is spent by marking prior sessions as unavailable for Callbacks. Each session is worth an experience point.

## Necessity

When a character wants to roll another die they can spend a plot point to add something plausible at D4. If they succeed at the test they may spend an experience point to make it permanent at D6. This does not apply to special and unique powers. 

\pagebreak

![](img/elevated2_bw.png)

## Advancing

Common uses of experience:

| Cost | Advance |
|:---|:---|
| 1 | Replace a Distinction or Moment with a new one |
| ½ old die | Step up Attribute, Tuning or Stranger Power |
| 1 | Step up a Connection |
| 1 | Signature Asset D6 |
| 1 / Step | Increase Signature Asset |
| -1 | New Permanent Complication D6, Step Up existing permanent Complication |
| 1 | Step Down Permanent Complication, goes away at D4 |
| 2 | New Stranger Power at D4 |

A new Stranger Power begins at D4, costing 2, provided the character has the requisite D8 in Tuning.

Stepping up Tuning past D12 always costs six, because that is the last die.

Experience may also be used to create a Customized Ability. These should have both Gain and Hinder effects. Unlike a Distinction the Hinder should be triggered by the situation and does not grant a plot point. The GM will assign the value, and determine if it is step-able or has a single cost.


