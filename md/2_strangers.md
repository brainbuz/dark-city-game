﻿![](img/mr_hand_banner.png)

# Strangers

The Strangers are ectoplasmic encephalopod like beings that can inhabit the shell of a formerly living being. They are literally walking corpses. They have the ability to Tune the machine which powers the city. They can also use it to amplify their psychic powers.

The Strangers have depleted the worlds the conquered leaving their home-world long ago. Such would have been earth’s fate, but the environment is toxic to them, and it’s inhabitants are poisonous to them. They are in decline and covet the Soul of Humanity as a means to reverse it. They hate the Humans and yet want to be more like them. 

Many of the strangers were thrown into space or died during Murdoch’s takeover. Other Strangers were able to flee into the bowels of the Machine. There was no place the strangers cast out could go, and those that could returned before their shells become frozen tombs for the creature inside.

Strangers operate individually and accumulate knowledge that is unique to the Stranger. In their native environment they can share their experiences with their collective. Being encased in corpses they’ve converted to use as shells, separates them, it interferes with their transference, but they still have a connection to those of their kind around them. When one of them dies or is in great pain, all Strangers nearby feel it. They can punish one of their own by cutting them off from that connection.

Strangers generally take single words, usually nouns, for their names. It is unknown why all Strangers seen in the movie had Male shells. For the game this is interpreted as a preference, that in needing to map themselves to human gender, they chose male; female shells are suitable but have been rarely chosen.

For Strangers the VITALITY rating applies only to their Shell, if the Shell is taken out by physical stress the Stranger also takes physical stress. Since the shell is dead, Strangers can take time out to use Tuning:Manifestation to attempt to repair their Shell. If the Shell is destroyed (damaged to what would be death) or the Stranger otherwise forced to leave it, in air they take 1 physical stress from exposure every beat. The amount of physical stress a Stranger essence can withstand is the greater of 3 or their Tuning – 3 steps.

## Detecting Strangers

Strangers stand out. When interacting with Humans they must use their Charm against the Human’s Wits to pass as Human. 

Humans with a Tuning Rating can feel the presence of Strangers, they get dice for WITS and Tuning, the difficulty is 7, really strong Tuners are harder to miss, any pluses the Stranger has to Tuning reduce the target for the Human sensing them. When a Human detects a Stranger this way they automatically know the Stranger’s name. Humans who Tune at a D6 or higher are similarly detectable by other Tuners, but their name isn’t automatically known.

## Stranger Advantages and Disadvantages

Strangers begin the game with a higher Tune rating and with all tree standard Stranger Powers.

Strangers Soul Distinction is shared by all of them. 

Strangers start with 1 less ability than Humans and cannot have CHARM greater than D8. They may also step CHARM and VITALITY as low as a D3. 

Sunlight, Oxygen, and Water all damage the Stranger. While the shell protects against all three, Strangers are extremely uncomfortable around water and sunlight.

## The Stranger Distinction

### Shared Soul of the Strangers

Gain: When working with another stranger(s) gain D8 on cooperative tasks or interactions.

Hinder: When working against other strangers add a D4 and receive a plot point.

## Connections

Strangers share a soul and have a natural connection with each other. Only Strangers with the Agent Role can have imprinted connections to humans when Murdoch takes over, these are treated as Assets for the Stranger. Other Strangers may be able to develop them later, but not initially.

## Stranger Roles

### Planner

Planned the City and the Experiments, evaluated the results. Gain this role’s die for knowledge tests involving a Human’s Role or the design of the City.

### Enforcer

Moved through the City implementing the Plan, recovering Humans in need of imprints, performing local adjustments to the environment. Gain this role’s die when imposing the plan. 

### Tuner

Worked with Mr. Book at the heart of the machine. When working directly with the machine use this role to step up the characters TUNING rating by the steps above D4 instead of taking its’ die. With the role value of D8, a Stranger with a D8 TUNING would have a D12 when inside the machine.

### Agent

Interacted with the subjects during experiments. When attempting to persuade a Human to follow script or provide information (or just that the Human is interacting with a Human), the stranger may add the die for this role. 

### Fabricator

Produced artifacts and material needed for the experiment.

### Technician

This character knows the inner workings of the machine and the Stranger infrastructure. The machine itself is self-maintaining, but that doesn’t mean it never needs attention. You know more about the Under City and the Machines than most Strangers and you know have a specialty area, such as the Imprint Lab that you know a great deal about, you have the skill to help Dr. Shreber repair it

# Stranger Powers:

To develop Stranger powers a Human must have a TUNING rating of at least D8. 

##  MENTAL CONTROL

The Strangers gain dice for their rating in MENTAL CONTROL, their TUNING ability and their WILL. Humans may resist with their TUNING ability and WILL. 
A minimum D6 effect die is required, even when the human has no effect die.

### Command

The Stranger speaks a one-word command, which the subject must obey. The order must be clear and straightforward: run, agree, fall, yawn, jump, laugh, surrender, stop, scream, follow. If the command is at all confusing or ambiguous, the subject may respond slowly or perform the task poorly. The subject cannot be ordered to do something directly harmful to herself, so a command like “die” is ineffective. 

### Suggestion

The stranger can verbally implant a false thought or hypnotic suggestion in the subject’s subconscious mind. Both stranger and target must be free from distraction, intense concentration and precise wording are required to be effective. The stranger may activate the imposed thought immediately or establish a stimulus that will trigger it later. The victim must be able to understand. Anything from simple, precise directives (handing over an item) to complex, highly involved ones (taking notes of someone’s habits and relaying that information at an appointed time). While a subject can have only one suggestion implanted at any time, multiple memories may be altered.

A human with a TUNING D4 or greater may attempt to resist a second time when the trigger is delayed. Altered memories may be retested if a situation arises that challenges the memory. 

## KINESIS

![](img/stranger.jpeg)

### Strength

While Strangers, inhabiting corpses, have less VITALITY than humans they can use psycho-kinetic power to create the effect of greater strength. For each Step of Kinesis above D4 the Stranger advances their VITALITY die one step in any contest of Strength.

### Telekinsis

With a rating of less than D8 the Stranger could only channel their telekinetic powers through their shell to increase its effective strength. With a rating of D8 or greater the Stranger can make objects fly. Making a person sized object fly requires a D8 or greater effect die with a difficulty of 8.

![](img/galleryofstrangers.png)

## MANIFESTATION

The power of changing the environment through the power of the machine. So long as the Tuner is able to continue succeeding on successive attempts they can keep adding to the effect until they have enough effect to accomplish their goal. Each Heroic success adds a multiplier of 1x to the current effect die for inclusion towards the goal.

MANIFESTATION cannot alter or create living things. It can replicate dead organisms, such as cotton, leather or dried beans.

Manifestation requires the accumulation of effects, where each step on the accumulated effects die brings completion closer.

| Difficulty | Effect Dice Steps | Task |
|---|---|---|
| 8| 4 | Create or Alter Small object up to size of a book |
| 9 | 4 | Repair 1 level of physical stress to shell |
| 10 | 5 | Larger object such as adding or removing a door |
| 12 | 6 | Alter a room or create a human sized object |
| 15 | 8 | Create a car sized object |
| 20 | 12 | Create or revise a building |
| 25 | 100 | Change a neighborhood |

> Shell Repair: The stranger’s shell is a corpse that has been made to be a life support environment for the stranger; while not living, it is organic and very advanced. If the stranger has any hitches on a field repair that does not succeed, the shell cannot be repaired in the field. Failure to field repair requires removing the stranger in a stranger safe environment and a Technician attempting to fix the shell. If the Technician fails the shell needs to be replaced. 

> From a game balance standpoint a replacement shell will always match the existing VITALITY, unless the character is re balanced or spending experience.

When Strangers work as a group on Manifestation they designate a leader, the others work against a difficulty of 8, any effects above D6 become pluses to the Leader to add to his Dice. In the movie there were scenes where possibly hundreds of strangers were contributing to Mr. Book for a night’s Tuning.

Murdoch is unique in that he can exert total control over the machine. When the Strangers and Murdoch are at odds there is a contest of WILL + TUNING between the leader and Murdoch to control the machine.

## Rare Stranger Powers

While all Strangers begin with the 3 common powers, most do not possess these powers.

### Time Control

Allows speeding time forward or slowing it for a limited area. When the Strangers need to create living things such as fresh vegetables or a tree for a park (which there are very few of in the City) they use this power to grow it extremely quickly. 

The difficulty is the same as manifesting a thing the size of the area in which to dialate time. For step side of effect Dice above D4 time may be frozen or accelerated by one day within that area.

### Life Control

Only available to Humans and Strangers who have left the collective and developed Empathy.

This may be used to heal or harm a living thing. 

Establish the complication level of to be healed, double that for the difficulty dice, match or beat the complication level with the effect dice to succeed. 

For doing harm the target resists with VITALITY + TUNING, if they have a TUNING of 6 or greater also add WILL to their resist. Targeting a Stranger inside their shell is a D6 complication.


