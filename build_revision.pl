#!/usr/bin/env perl

use 5.032;
use Path::Tiny;
use Time::Piece;

my $t = localtime;
my $printdate = $t->strftime( "%e %B %Y");
my $isodate = $t->ymd;

say "Generating Releases for $printdate $isodate";

for my $edition ( qw /playersguide gmguide/) {
  my $adocfile = "adoc/$edition.adoc";
  my $pdffile = "release/$edition\_$isodate.pdf";
  my $epub = "release/$edition\_$isodate.epub";
  my $html = "release/$edition\_$isodate.html";
  my $adoc = path($adocfile)->slurp();
  for( $adocfile, $pdffile, $epub, $html ) { say $_ };
  $adoc =~ s/:revdate:.*\n/:revdate: $printdate\n/;
  $adoc =~ s/:docdate:.*\n/:docdate: $printdate\n/;
  path( $adocfile )->spew( $adoc);
  system( "asciidoctor-pdf -a optimize -o $pdffile $adocfile");
  system( "asciidoctor -o $html -b html5 $adocfile");
  system( "asciidoctor-epub3 -o $epub $adocfile");
}

my $index = qq |

<html>
<HEAD>
<TITLE>Documents on Brainbuz</TITLE>
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
<style>
body{
  background-color: LightSteelBlue;
}
.container::before{
  content:"";
  background-image: url('strangers_withmodel.png');
  background-size:cover;
  background-repeat: no-repeat;
  background-attachment: fixed;
  opacity:0.3;
  position:absolute;
  top:0;
  bottom:0;
  right:0;
  left:0;
  width:auto;
  height:auto;
  z-index:-3;
}

.container {
  color: MidnightBlue;
  height: 100%;
  width: 100%;
  position: relative;
}

.vertical-center {
  text-align: center;
  margin-left: 25%;
  position: absolute;
  top: 50%;

  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
</style>
</HEAD>
<body>


<div class="container">
  <div class="vertical-center">
    <h1>DarkCity RPG Documents</h1>


    <h1><a href="playersguide_$isodate.pdf">PDF Dark City Player&rsquo;s Guide $printdate</a></h1>

    <h1><a href="playersguide_$isodate.epub">EPUB Dark City Player&rsquo;s Guide $printdate</a></h1>

    <h1><a href="playersguide_$isodate.html">View In Browser Dark City Player&rsquo;s Guide $printdate</a></h1>

    <h1><a href="DarkCityCharacterSheet.docx">Character Sheet</a></h1><br /><h2>Sheet Only works with MS Word Local
    Install.</h2>
</div>
</div>
</body>
</html>

|;

system( 'rsync -r release/ /more/website/docs/' );
path( '/more/website/docs/darkcity.html')->spew($index);
